/* 
 * File:   mrgss.h
 * Author: manuel
 *
 * Created on 24 de septiembre de 2015, 19:57
 */

#ifndef MRGSS_H
#define	MRGSS_H
#include <mruby.h>
#include <mruby/class.h>

#ifdef	__cplusplus
extern "C" {
#endif
    struct RClass* mrgss_get_module(mrb_state* mrb);
    struct RClass* mrgss_create_class(mrb_state *mrb, const char *name);
    struct RClass* mrgss_get_class(mrb_state *mrb, const char *name);

    void mrgss_iv_create(mrb_state *mrb, mrb_value object, const char *name, mrb_value value);
    mrb_value mrgss_iv_get(mrb_state *mrb,mrb_value object, const char *name);
    
    void mrgss_raise(mrb_state *mrb, struct RClass *exception, const char *message);

#ifdef	__cplusplus
}
#endif

#endif	/* MRGSS_H */

